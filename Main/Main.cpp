#include "Huffman File.h"


int main()
{
	string file_out;
	unsigned int size;
	unsigned int choice;

	cout << "MENU" << endl;
	cout << "1. Nen cac file trong folder" << endl;
	cout << "2. Xem noi dung cac file da nen" << endl;
	cout << "3. Giai nen tat ca file" << endl;
	cout << "4. Chon va giai nen cac file" << endl;
	cout << "0. Thoat" << endl;
	cout << endl;
	cout << "Lua chon cua ban: ";
	cin >> choice;

	while (choice != 0) {
		if (choice == 1)
			EncodeFile(file_out, size);
		else if (choice == 2)
			ReadFileInfo(file_out, size);
		else
			DecodeFile(file_out, size, choice);
		cout << endl;
		cout << "Lua chon cua ban: ";
		cin >> choice;
	}
	return 0;
}