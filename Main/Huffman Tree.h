#pragma once
#include "Huffman File.h"

void createFreqTable(unsigned int freqTable[], string FileName); //tao bang tan so
bool compare(NodeHeap* l, NodeHeap* r); //so sanh 2 node
HeapTable* createFreqTree(unsigned int freqTable[]); // tao bang huffman
void swapNodeHeap(NodeHeap** a, NodeHeap** b); // doi cho 2 node
void Heapify(HeapTable* Tree, int idx); //thao tac heapify
void BuildHeap(HeapTable* Tree); //build minheap
NodeHeap* pop(HeapTable* Tree); //pop 1 phan tu ra
void push(HeapTable* Tree, NodeHeap* a); //push 1 phan tu vo lai cay
void buildHuffManTree(HeapTable* Tree); //tao cay huffman
void createBitCode(NodeHeap* a, string& way, unsigned char& c, string BitCode[]); //tao bitcode cho ky tu