#pragma once
#include "targetver.h"
#include <iostream>
#include <string>
#include <fstream>
#include <bitset>
#include <Windows.h>
#include <iomanip>
using namespace std;

struct File
{
	char Name[256];
	unsigned int Size_Before;
	unsigned int Size_After;
	unsigned int Start;
	unsigned int UnusedBit;
};

struct FileHeader
{
	char NameFile[3];
	unsigned int freqTable[256];
	unsigned int NumOfFile;
};

struct NodeHeap
{
	unsigned char c; //ky tu
	unsigned int freq; //tan so xuat hien
	NodeHeap *left, *right;
};

struct HeapTable
{
	NodeHeap** arr; // mang 2 chieu
	unsigned int size; //current size cua mang arr
	unsigned int fullsize; //max size cua mang arr
};

NodeHeap* createNewNode(unsigned char c, unsigned int freq); //tao node moi
FileHeader* createFileHeader(string InputPath);
File* createFile(string InputPath, string InputFile);
void TakeFileNameInFolder(string InputPath, string* FileName);
ofstream& GoToFile(ofstream& FileOutput, unsigned int n);
void writeFile(string InputPath, string OutputPath, string OutputFile, string* FileName, unsigned int size, string BitCode[], FileHeader* Header);
void EncodeFile(string& file_out, unsigned int& size); //nen file
void DecodeFile(string file_out, unsigned int size, unsigned int choice); //giai nen file
void ReadFileInfo(string file_out, unsigned int size);