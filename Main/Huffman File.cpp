#include "Huffman File.h"
#include "Huffman Tree.h"
#include "Huffman Code.h"
#include "FindFile.h"

NodeHeap* createNewNode(unsigned char c, unsigned int freq)
{
	NodeHeap* tmp = new NodeHeap;
	tmp->c = c;
	tmp->freq = freq;
	tmp->left = tmp->right = NULL;
	return tmp;
}

FileHeader* createFileHeader(string InputPath)
{
	string data;
	InputPath += "\\*";
	HANDLE hFind;
	WIN32_FIND_DATAA data2;
	hFind = FindFirstFileA(InputPath.c_str(), &data2);
	int NumOfFile = 0;
	if (hFind != INVALID_HANDLE_VALUE)
	{
		do {
			if (data2.dwFileAttributes != FILE_ATTRIBUTE_DIRECTORY)
				NumOfFile++;
		} while (FindNextFileA(hFind, &data2));
		FindClose(hFind);
	}
	FileHeader* tmp = new FileHeader;
	tmp->NumOfFile = NumOfFile;
	tmp->NameFile[0] = 'D';
	tmp->NameFile[1] = 'S';
	tmp->NameFile[2] = 'T';
	tmp->NameFile[3] = '\0';
	return tmp;
}

File* createFile(string InputPath, string InputFile)
{
	File* tmp = new File;
	for (int i = 0; i < InputFile.size(); i++)
		tmp->Name[i] = InputFile[i];
	tmp->Name[InputFile.size()] = '\0';

	size_t size = 0;
	ifstream fin;
	fin.open((InputPath + InputFile).c_str(), ios::binary);
	if (fin.is_open()) {
		fin.seekg(0, ios::end);
		size = fin.tellg();
		fin.seekg(0, ios::beg);
	}
	fin.close();
	tmp->Size_Before = size;
	tmp->Size_After = 0;
	tmp->Start = 0;
	tmp->UnusedBit = 0;
	return tmp;
}

void TakeFileNameInFolder(string InputPath, string* FileName)
{
	string data;
	InputPath += "\\*";
	HANDLE hFind;
	WIN32_FIND_DATAA data2;
	hFind = FindFirstFileA(InputPath.c_str(), &data2);
	int i = 0;
	if (hFind != INVALID_HANDLE_VALUE)
	{
		do {
			if (data2.dwFileAttributes != FILE_ATTRIBUTE_DIRECTORY)
				FileName[i++] += data2.cFileName;
		} while (FindNextFileA(hFind, &data2));
		FindClose(hFind);
	}
}

ofstream& GoToFile(ofstream& FileOutput, unsigned int n)
{
	FileOutput.seekp(0, ios::beg);
	unsigned int pos = FileOutput.tellp();
	FileOutput.seekp(pos + sizeof(struct FileHeader) + n * sizeof(struct File));
	return FileOutput;
}

void writeFile(string InputPath, string OutputPath, string OutputFile, string* FileName, unsigned int size, string BitCode[], FileHeader* Header)
{
	ofstream fout(OutputPath + OutputFile, ios::binary);
	fout.write((char*)Header, sizeof(struct FileHeader));
	for (int i = 0; i < size; i++) {
		File* file = createFile(InputPath, FileName[i]);
		fout.write((char*)file, sizeof(struct File));
	}
	fout.close();

	for (int i = 0; i < size; i++) {
		File* file = new File;
		string filepath = InputPath + FileName[i];
		encode(BitCode, filepath, (OutputPath + OutputFile), file);
		for (int j = 0; j < FileName[i].size(); j++)
			file->Name[j] = FileName[i][j];
		file->Name[FileName[i].size()] = '\0';
		ofstream fout(OutputPath + OutputFile, ios::in | ios::binary);
		GoToFile(fout, i);
		fout.write((char*)file, sizeof(struct File));
		fout.close();
	}
	cout << "Encode complete!" << endl;
}

void EncodeFile(string& file_out, unsigned int& size)
{
	HeapTable* H = NULL; //cay Huffman H
	unsigned int freqTable[256]; //bang tan so
	for (int i = 0; i < 256; i++)
		freqTable[i] = 0;
	string BitCode[256]; //luu bitcode
	string InputPath, OutputPath, FileInput, FileOutput, Way;
	string* FileName; //mang chua ten cac file trong folder can nen
	unsigned char c;

	cout << "Duong dan den folder can nen: "; //duong dan den folder chua cac file can nen
	cin.ignore();
	getline(cin, InputPath);
	cout << "Duong dan den folder chua file nen: "; //duong dan den folder chua file nen
	getline(cin, OutputPath);
	cout << "Ten file nen: "; //ten file nen
	getline(cin, FileOutput);

	FileOutput += ".dst";
	file_out = OutputPath + FileOutput;
	FileHeader* Header = createFileHeader(InputPath);
	size = Header->NumOfFile;
	if (size == 0) {
		cout << "Folder rong" << endl;
		return;
	}
	FileName = new string[Header->NumOfFile];
	TakeFileNameInFolder(InputPath, FileName);

	for (int i = 0; i < Header->NumOfFile; i++) {
		FileInput = InputPath + FileName[i];
		createFreqTable(freqTable, FileInput);
	}
	for (int i = 0; i < 256; i++)
		Header->freqTable[i] = freqTable[i];

	H = createFreqTree(freqTable);
	buildHuffManTree(H);
	createBitCode(H->arr[0], Way, c, BitCode);

	writeFile(InputPath, OutputPath, FileOutput, FileName, Header->NumOfFile, BitCode, Header);

	delete[]FileName;
}

void DecodeFile(string file_out, unsigned int size, unsigned int choice)
{
	HeapTable* H = NULL;
	string BitCode[256];
	string OutputPath, Way;
	unsigned char c;

	ifstream fin((file_out).c_str(), ios::binary);
	if (fin.is_open()) {
		FileHeader* Head = new FileHeader;
		fin.read((char*)Head, sizeof(struct FileHeader));
		if (Head->NameFile[0] == 'D' && Head->NameFile[1] == 'S' && Head->NameFile[2] == 'T') {
			cout << "Nhap dia chi folder giai nen: ";
			cin.ignore();
			getline(cin, OutputPath);

			H = createFreqTree(Head->freqTable);
			buildHuffManTree(H);
			createBitCode(H->arr[0], Way, c, BitCode);

			if (choice == 3) {
				for (int i = 0; i < size; i++) {
					File* file = new File;
					fin.read((char*)file, sizeof(struct File));
					unsigned int pos = fin.tellg();
					fin.seekg(file->Start);
					decode(H, fin, file, OutputPath);
					fin.seekg(pos);
				}
			}
			else {
				string fileEx;
				cout << "Nhap thu tu cac file muon giai nen: ";
				getline(cin, fileEx);
				int i = 0;
				int headerpos = fin.tellg();
				while (i < fileEx.size()) {
					File* file = new File;
					fin.seekg(headerpos + ((fileEx[i] - '0' - 1) * sizeof(struct File)));
					fin.read((char*)file, sizeof(struct File));
					unsigned int pos = fin.tellg();
					fin.seekg(file->Start);
					decode(H, fin, file, OutputPath);
					fin.seekg(pos);
					i += 2;
				}
			}
		}
		else
			cout << "File sai!!!" << endl;
	}
	else
		cout << "Loi file nen" << endl;
	fin.close();
}

void ReadFileInfo(string file_out, unsigned int size)
{

	ifstream fin(file_out.c_str(), ios::binary);
	fin.seekg(0, ios::beg);
	fin.seekg(sizeof(struct FileHeader));
	cout << left;
	cout << setw(7) << "STT" << setw(30) << "Ten file" << setw(40) << "Size truoc nen (bytes)" << "Size sau nen (bytes)" << endl;
	for (int i = 0; i < size; i++) {
		File* file = new File;
		fin.read((char*)file, sizeof(struct File));
		int j = 0;
		string name;
		while (file->Name[j] != '\0')
			name+= file->Name[j++];
		cout << setw(7) << i + 1 << setw(30) << name << setw(40) << file->Size_Before << file->Size_After << endl;
	}
	fin.close();
}