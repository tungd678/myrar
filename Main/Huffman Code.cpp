#include "Huffman File.h"
#include "Huffman Code.h"

void encode(string BitCode[], string FileName, string FileOutput, File* & file)
{
	string data;
	unsigned char* DATA = 0;
	size_t size = 0;

	ifstream fin(FileName.c_str(), ios::binary);
	if (fin.is_open()) {
		fin.seekg(0, ios::end);
		size = fin.tellg();
		fin.seekg(0, ios::beg);
		DATA = new unsigned char[size];
		fin.read((char*)DATA, size);
	}
	fin.close();
	file->Size_Before = size;

	ofstream fout(FileOutput.c_str(), ios::binary | ios::app);
	fout.seekp(0, ios::end);
	file->Start = fout.tellp();
	int i = 0;
	while (i < size) {
		unsigned char c = 0;
		data += BitCode[(int)DATA[i]];
		if (i == size - 1) {
			int loop = data.size() / 8;
			for (int j = 0; j < loop; j++) {
				for (int k = 0; k < 8; k++)
					c |= ((data[k] - '0') << (7 - k));
				fout.write((char*)&c, sizeof(char));
				c = 0;
				data.erase(0, 8);
			}
			if (data.size() == 0)
				file->UnusedBit = 0;
			else {
				for (int j = 0; j < data.size(); j++)
					c |= ((data[j] - '0') << (7 - j));
				fout.write((char*)&c, sizeof(char));
				file->UnusedBit = 8 - data.size();
			}
			unsigned int curlocal = fout.tellp();
			file->Size_After = curlocal - file->Start;
			break;
		}
		else if (data.size() <= 8) {
			if (data.size() == 8) {
				for (int j = 0; j<data.size(); j++)
					c |= ((data[j] - '0') << (7 - j));
				fout.write((char*)&c, sizeof(char));
				c = 0;
				data.clear();
			}
			i++;
		}
		else {
			int loop = data.size() / 8;
			for (int j = 0; j < loop; j++) {
				for (int k = 0; k<8; k++)
					c |= ((data[k] - '0') << (7 - k));
				fout.write((char*)&c, sizeof(char));
				c = 0;
				data.erase(0, 8);
			}
			i++;
		}
	}

	fout.close();
	delete[]DATA;
}

void decode(HeapTable* Tree, ifstream& fin, File* file, string OutputPath)
{
	string data, name;
	unsigned char* DATA = 0;
	DATA = new unsigned char[file->Size_After];
	fin.read((char*)DATA, file->Size_After);
	
	int x = 0;
	while (file->Name[x] != '\0')
		name += file->Name[x++];

	ofstream fout;
	fout.open((OutputPath + name).c_str(), ios::binary);

	for (int i = 0; i < file->Size_After - 1; i++)
		data += bitset<8>(DATA[i]).to_string();
	data.erase(data.size() - file->UnusedBit);
	delete[]DATA;

	int i = 0;
	while (i < data.size()) {
		NodeHeap* cur = Tree->arr[0];
		while (cur->left != NULL && cur->right != NULL) {
			if (data[i] == '0')
				cur = cur->left;
			else
				cur = cur->right;
			i++;
		}
		fout.write((char*)&(cur->c), sizeof(char));
	}
	int size = fout.tellp();
	if (size + 1 == file->Size_Before) {
		int i = 0;
		while (file->Name[i] != '\0')
			cout << file->Name[i++];
		cout << ": giai nen thanh cong" << endl;
	}
	else
	{
		int i = 0;
		while (file->Name[i] != '\0')
			cout << file->Name[i];
		cout << ": File loi" << endl;
	}
	fout.close();
}