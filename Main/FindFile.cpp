#include "Huffman File.h"
#include "FindFile.h"

wstring string_to_wstring(const std::string& text) {
	return std::wstring(text.begin(), text.end());
}