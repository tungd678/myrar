#include "Huffman File.h"
#include "Huffman Tree.h"

void createFreqTable(unsigned int freqTable[], string FileName)
{
	ifstream fin;
	fin.open(FileName.c_str(), ios::binary);
	string data;
	size_t size = 0;

	if (fin.is_open())
	{
		unsigned char* DATA;
		fin.seekg(0, ios::end);
		size = fin.tellg();
		fin.seekg(0, ios::beg);
		DATA = new unsigned char[size];
		fin.read((char*)DATA, size);

		for (int i = 0; i < size; i++) {
			freqTable[DATA[i]]++;
		}
		delete[]DATA;
	}
	fin.close();
}

bool compare(NodeHeap* l, NodeHeap* r)
{
	if (l->freq < r->freq)
		return true;
	else if (l->freq > r->freq)
		return false;
	else if (l->freq == r->freq && l->c < r->c)
		return true;
	else
		return false;
}

HeapTable* createFreqTree(unsigned int freqTable[])
{
	HeapTable* Tree = new HeapTable;
	unsigned int size = 0;
	for (int i = 0; i < 256; i++)
		if (freqTable[i] != 0)
			size++;
	Tree->size = size;
	Tree->fullsize = size;
	Tree->arr = new NodeHeap*[Tree->fullsize];

	unsigned int cur = 0;
	for (int i = 0; i < 256; i++) {
		if (freqTable[i] != 0)
			Tree->arr[cur++] = createNewNode((unsigned char)i, freqTable[i]);
	}
	return Tree;
}

void swapNodeHeap(NodeHeap** a, NodeHeap** b)
{
	NodeHeap* tmp = *a;
	*a = *b;
	*b = tmp;
}

void Heapify(HeapTable* Tree, int idx)
{
	int smaller = idx;
	int left = 2 * idx + 1;
	int right = 2 * idx + 2;

	if (left < Tree->size && compare(Tree->arr[left], Tree->arr[smaller]))
		smaller = left;
	if (right < Tree->size && compare(Tree->arr[right], Tree->arr[smaller]))
		smaller = right;
	if (smaller != idx) {
		swapNodeHeap(&Tree->arr[idx], &Tree->arr[smaller]);
		Heapify(Tree, smaller);
	}
}

void BuildHeap(HeapTable* Tree)
{
	for (int i = (Tree->size) / 2 - 1; i >= 0; i--)
		Heapify(Tree, i);
}

NodeHeap* pop(HeapTable* Tree)
{
	NodeHeap* tmp = Tree->arr[0];
	Tree->arr[0] = Tree->arr[Tree->size - 1];
	--Tree->size;
	Heapify(Tree, 0);
	return tmp;
}

void push(HeapTable* Tree, NodeHeap* a)
{
	Tree->size++;
	Tree->arr[Tree->size - 1] = a;
	BuildHeap(Tree);
}

void buildHuffManTree(HeapTable* Tree)
{
	while (Tree->size != 1) {
		BuildHeap(Tree);
		NodeHeap* tmp1 = pop(Tree);
		NodeHeap* tmp2 = pop(Tree);
		NodeHeap* a;
		if (compare(tmp1, tmp2)) {
			a = createNewNode(tmp1->c, tmp1->freq + tmp2->freq);
			a->left = tmp1;
			a->right = tmp2;
		}
		else {
			a = createNewNode(tmp2->c, tmp1->freq + tmp2->freq);
			a->left = tmp2;
			a->right = tmp1;
		}
		push(Tree, a);
	}
}

void createBitCode(NodeHeap* a, string& way, unsigned char& c, string BitCode[])
{
	if (a->left == NULL && a->right == NULL) {
		BitCode[(int)a->c] = way;
		return;
	}
	if (a->left) {
		way += '0';
		a->c = c;
		createBitCode(a->left, way, c, BitCode);
		way.erase(way.size() - 1);
	}
	if (a->right) {
		way += '1';
		a->c = c;
		createBitCode(a->right, way, c, BitCode);
		way.erase(way.size() - 1);
	}
}